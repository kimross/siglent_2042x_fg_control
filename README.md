This python3 program provides a frequency generator remote control including GUI, which
is especially designed for Siglent SDG2042X but should work with any device
sharing the same SCPI command syntax.

The frequency generator (device) has to be connected via ethernet and the IP address
has to be changed inside the program.

The functionalities of the program are:
- Read and display of output status of channels 1 and 2
- Read and display of current waveform, frequency, amplitude and phase of channels 1 and 2
- Setting the frequency, amplitude, phase and output status for channels 1 and 2 individually
- Writing log files (the default folder is ./fg_log_files/), this function can be disabled.