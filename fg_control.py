#Copyright (C) 2020 Kim Ulrike Ross
#
#This program is free software: you can redistribute it and/or modify it under the terms of the MIT License.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
#implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the MIT License for more details.
#
#You should have received a copy of the MIT License along with this program. If not, see <http://opensource.org/licenses/MIT>




# Import packages

import socket
import time
from time import localtime, strftime
from PyQt5 import QtWidgets, uic, QtCore
import sys
import select
import os


# Writing log file. Set to false if no file shall be written
write_log = True

if write_log == True:
    if not os.path.exists('./fg_log_files'):
        os.makedirs('./fg_log_files')
    filename = strftime("%Y%m%d", localtime()) + "_fg_log"
    outfile = open("./fg_log_files/" + filename, 'a', buffering = 1)


# Define LED colors

color_grey = "background-color: qradialgradient(spread:pad, cx:0.521, cy:0.5, radius:0.5, fx:0.418, fy:0.346, stop:0 rgba(221, 221, 221, 251), stop:0.601036 rgba(122, 122, 122, 205), stop:0.756477 rgba(88, 88, 88, 215), stop:0.854922 rgba(255, 0, 0, 0));"
color_red = "background-color: qradialgradient(spread:pad, cx:0.521, cy:0.5, radius:0.5, fx:0.418, fy:0.346, stop:0 rgba(221, 0, 0, 130), stop:0.601036 rgba(255, 0, 0, 205), stop:0.756477 rgba(109, 0, 0, 215), stop:0.932642 rgba(255, 0, 0, 0));"
color_green = "background-color: qradialgradient(spread:pad, cx:0.521, cy:0.5, radius:0.5, fx:0.418, fy:0.346, stop:0 rgba(70, 221, 0, 130), stop:0.601036 rgba(0, 228, 23, 205), stop:0.756477 rgba(0, 110, 45, 215), stop:0.911917 rgba(255, 0, 0, 0));"


# Define functions

# Read basic waveform
def get_BSWV(dev):
    try:
        recv = str(dev.recv(buffer_size), 'utf-8')
    
    except:
        print("Error receiving waveform data.")
    
    try:
        recv_splt = recv.split(",")
        wavetype = recv_splt[1]
        vpp = float(recv_splt[7].replace("V", ""))*1000
        freq = float(recv_splt[3].replace("HZ", ""))/1e6
        phase = float(recv_splt[19].split('\n')[0])
        
        return wavetype, vpp, freq, phase
    
    except:
        print("Error in data format, values could not be read.")
        
        return "", 0., 0., 0. 


# Read output
def get_output(dev):
    try:
        recv = str(dev.recv(buffer_size), 'utf-8')
    
    except:
        print("Error receiving waveform data.")
    
    try:
        recv_splt = recv.split()
        output_on_off = recv_splt[1].split(",")[0]
        if output_on_off == "ON":
            return 1
        else:
            return 0

    except:
        print("Error in data format, values could not be read.")
        
        return None


# Read socket buffer
def read_buffer(sock):
    i, o, e = select.select([sock], [], [], 0.1) #check if input available
    if i:
        ans = sock.recv(buffer_size)

    return ans



class ThreadClass(QtCore.QObject):

    # Define signals
    ch1_output_signal = QtCore.pyqtSignal(int)
    ch2_output_signal = QtCore.pyqtSignal(int)
    ch1_waveform_signal = QtCore.pyqtSignal(str)
    ch2_waveform_signal = QtCore.pyqtSignal(str)
    ch1_amplitude_signal = QtCore.pyqtSignal(float)
    ch2_amplitude_signal = QtCore.pyqtSignal(float)
    ch1_frequency_signal = QtCore.pyqtSignal(float)
    ch2_frequency_signal = QtCore.pyqtSignal(float)
    ch1_phase_signal = QtCore.pyqtSignal(float)
    ch2_phase_signal = QtCore.pyqtSignal(float)


    # Initialise
    def __init__(self):
        super(ThreadClass, self).__init__()

        self.t = int(time.time())
        self.temp_time = self.t

        # Enable reading loop
        self.ready = True
        # Disable counter for writing log file in reading loop
        self.write = 0
    

    def run(self):
        # Initialise parameters
        ch1_output = 0
        ch2_output = 0
        ch1_waveform = ""
        ch2_waveform = ""
        ch1_amplitude = 0.
        ch2_amplitude = 0.
        ch1_frequency = 0.
        ch2_frequency = 0.
        ch1_phase = 0.
        ch2_phase = 0.

        while 1:
            # read current settings every second
            if self.temp_time >= self.t + 1:
                if self.ready == True:
                    # Disable using socket from other functions
                    self.ready = False
                    
                    # Read output channel 1
                    try:
                        dev.send(b'C1:OUTP?')
                        time.sleep(0.1)
                    except:
                        print("Error asking for output status on channel 1.")
                    ch1_output = get_output(dev)
                    if ch1_output == None:
                        continue
                    else:
                        self.ch1_output_signal.emit(ch1_output)
                    
                    # Read output channel 2
                    try:
                        dev.send(b'C2:OUTP?')
                        time.sleep(0.1)
                    except:
                        print("Error asking for output status on channel 2.")
                    ch2_output = get_output(dev)
                    if ch2_output == None:
                        continue
                    else:
                        self.ch2_output_signal.emit(ch2_output)
                    
                    # Read basic waveform channel 1
                    try:
                        dev.send(b'C1:BSWV?')
                        time.sleep(0.1)
                    except:
                        print("Error asking for waveform on channel 1.")
                    ch1_waveform, ch1_amplitude, ch1_frequency, ch1_phase = get_BSWV(dev)
                    if ch1_waveform == "":
                        continue
                    else:
                        self.ch1_waveform_signal.emit(ch1_waveform)
                        self.ch1_amplitude_signal.emit(ch1_amplitude)
                        self.ch1_frequency_signal.emit(ch1_frequency)
                        self.ch1_phase_signal.emit(ch1_phase)

                    # Read basic waveform channel 2
                    try:                   
                        dev.send(b'C2:BSWV?')
                        time.sleep(0.1)
                    except:
                        print("Error asking for waveform on channel 2.")
                    ch2_waveform, ch2_amplitude, ch2_frequency, ch2_phase = get_BSWV(dev)
                    if ch2_waveform == "":
                        continue
                    else:
                        self.ch2_waveform_signal.emit(ch2_waveform)
                        self.ch2_amplitude_signal.emit(ch2_amplitude)
                        self.ch2_frequency_signal.emit(ch2_frequency)
                        self.ch2_phase_signal.emit(ch2_phase)
                
                    
                    # If enabled, write log file
                    if write_log == True:
                        self.write = self.write*2
                        # Sometimes setting the output takes some seconds,
                        # therefore wait three seconds until writing log
                        if self.write == 8:
                            print("Writing log...")
                            if ch1_output == 0:
                                ch1_output_string = "OFF"
                            else:
                                ch1_output_string = "ON"

                            if ch2_output == 0:
                                ch2_output_string = "OFF"
                            else:
                                ch2_output_string = "ON"

                            outfile.write("Current settings:\n")
                            outfile.write(strftime("%Y%m%d_%H%M%S", localtime()) + '\t' + str(1) + '\t' + \
                                          str(ch1_frequency) + '\t' + str(ch1_amplitude) + '\t' + \
                                          str(ch1_phase) + '\t' + str(ch1_output_string) + '\n')
                            outfile.write(strftime("%Y%m%d_%H%M%S", localtime()) + '\t' + str(2) + '\t' + \
                                          str(ch2_frequency) + '\t' + str(ch2_amplitude) + '\t' + \
                                          str(ch2_phase) + '\t' + str(ch2_output_string) + '\n\n')
                            self.write = 0

                                    
                    self.ready = True
                    self.t = self.temp_time
                
                else:
                    continue

            else:
                self.temp_time = time.time()


    def set_values_ch1(self, output, frequency, amplitude, phase):

        # Prevent reading loop from interfering
        self.ready = False

        # If enabled, write settings to be sent
        if write_log == True:
            outfile.write("User input:\n")
            outfile.write(strftime("%Y%m%d_%H%M%S", localtime()) + '\t' + str(1) + '\t' + \
                         str(float(frequency)/1e6) + '\t' + str(amplitude) + '\t' + \
                          str(phase) + '\t' + str(output) + '\n\n')

        print("\nConfiguring channel 1:")

        # Set output
        message = "C1:OUTP "+output
        print("Output: " + output)
        try:
            dev.send(bytes(message, 'utf-8'))
        except:
            print("Error configuring channel 1 output.")
        time.sleep(0.1)

        # Set frequency
        message = "C1:BSWV FRQ,"+frequency
        print("Frequency: " + frequency + " MHz")
        try:
            dev.send(bytes(message, 'utf-8'))
        except:
            print("Error configuring channel 1 frequency.")
        time.sleep(0.1)

        # Set amplitude
        message = "C1:BSWV AMP,"+amplitude
        print("Amplitude: " + amplitude + " mV")
        try:
            dev.send(bytes(message, 'utf-8'))
        except:
            print("Error configuring channel 1 amplitude.")
        time.sleep(0.1)
        
        # Set phase
        message = "C1:BSWV PHSE,"+phase
        print("Phase: " + phase + " degrees")
        try:
            dev.send(bytes(message, 'utf-8'))
        except:
            print("Error configuring channel 1 phase.")
        time.sleep(0.1)

        # Continue with reading loop
        self.ready = True
        # Start counter for writing current settings in reading loop
        self.write = 1


    def set_values_ch2(self, output, frequency, amplitude, phase):
        
        # Prevent reading loop from interfering
        self.ready = False

        # If enabled, write settings to be sent
        if write_log == True:
            outfile.write("User input:\n")
            outfile.write(strftime("%Y%m%d_%H%M%S", localtime()) + '\t' + str(2) + '\t' + \
                         str(float(frequency)/1e6) + '\t' + str(amplitude) + '\t' + \
                          str(phase) + '\t' + str(output) + '\n\n')

        print("\nConfiguring channel 2:")

        # Set output
        message = "C2:OUTP "+output
        print("Output: " + output)
        try:
            dev.send(bytes(message, 'utf-8'))
        except:
            print("Error configuring channel 2 output.")
        time.sleep(0.1)

        # Set frequency
        message = "C2:BSWV FRQ,"+frequency
        print("Frequency: " + frequency + " MHz")
        try:
            dev.send(bytes(message, 'utf-8'))
        except:
            print("Error configuring channel 2 frequency.")
        time.sleep(0.1)

        # Set amplitude
        message = "C2:BSWV AMP,"+amplitude
        print("Amplitude: " + amplitude + " mV")
        try:
            dev.send(bytes(message, 'utf-8'))
        except:
            print("Error configuring channel 2 amplitude.")
        time.sleep(0.1)

        # Set phase
        message = "C2:BSWV PHSE,"+phase
        print("Phase: " + phase + " degrees")
        try:
            dev.send(bytes(message, 'utf-8'))
        except:
            print("Error configuring channel 2 phase.")
        time.sleep(0.1)

        # Continue with reading loop
        self.ready = True
        # Start counter for writing current settings in reading loop
        self.write = 1



class MainWindow(QtWidgets.QMainWindow, QtCore.QObject):
    
    
    def __init__(self):
        super(MainWindow, self).__init__()
        uic.loadUi('fg_control_gui.ui', self)

        # Load colors
        global color_grey
        global color_red
        global color_green

        
        # Set placeholder texts
        self.value_amplitude_ch1.setPlaceholderText("Unknown")        
        self.value_amplitude_ch2.setPlaceholderText("Unknown")        
        self.value_frequency_ch1.setPlaceholderText("Unknown")        
        self.value_frequency_ch2.setPlaceholderText("Unknown")        
        self.value_phase_ch1.setPlaceholderText("Unknown")        
        self.value_phase_ch2.setPlaceholderText("Unknown")        
        self.value_waveform_ch1.setText("Unknown")        
        self.value_waveform_ch2.setText("Unknown")        

        # Default LEDs
        self.LED_connected.setStyleSheet(color_red)
        self.LED_output_ch1.setStyleSheet(color_grey)
        self.LED_output_ch2.setStyleSheet(color_grey)


        # Initialise worker thread
        self.worker = ThreadClass()
        self.workerThread = QtCore.QThread()
        self.worker.moveToThread(self.workerThread)

        # Set defaults
        self.initialise()
        
        # Connect signals to widgets
        self.worker.ch1_output_signal.connect(self.update_output_ch1)
        self.worker.ch2_output_signal.connect(self.update_output_ch2)
        self.worker.ch1_waveform_signal.connect(self.update_waveform_ch1)
        self.worker.ch2_waveform_signal.connect(self.update_waveform_ch2)
        self.worker.ch1_amplitude_signal.connect(self.update_amplitude_ch1)
        self.worker.ch2_amplitude_signal.connect(self.update_amplitude_ch2)
        self.worker.ch1_frequency_signal.connect(self.update_frequency_ch1)
        self.worker.ch2_frequency_signal.connect(self.update_frequency_ch2)
        self.worker.ch1_phase_signal.connect(self.update_phase_ch1)
        self.worker.ch2_phase_signal.connect(self.update_phase_ch2)
        self.button_submit_ch1.clicked.connect(self.submit_configs_ch1)
        self.button_submit_ch2.clicked.connect(self.submit_configs_ch2)
        
        # Start worker thread
        self.workerThread.started.connect(self.worker.run)
        self.workerThread.start()

        self.show()


    # Redefinition of closeEvent for proper closing log file and socket connection
    def closeEvent(self, event):
        if write_log == True:
            outfile.close()
            print("Log file closed.")
        dev.close()
        print("Connection to frequency generator closed.")
        event.accept()


    def initialise(self):
        welcome = dev.recv(buffer_size)
        print(welcome)
        if b'SDG' in welcome:
            self.set_LED_connected(True)
        else:
            self.set_LED_connected(False)
        
        # Get current output status of channel 1
        try:
            dev.send(b'C1:OUTP?')
            time.sleep(0.1)
        except:
            print("Error asking for output status on channel 1.")
        ch1_output = get_output(dev)
        
        # Get current output status of channel 2
        try:
            dev.send(b'C2:OUTP?')
            time.sleep(0.1)
        except:
            print("Error asking for output status on channel 2.")
        ch2_output = get_output(dev)
        
        # Case if output could not be read
        if ch1_output == None:
            ch1_output = 0
        elif ch2_output == None:
            ch2_output = 0
        else:
            self.set_combo_box_default(ch1_output, ch2_output)



    # Set output selection default to current status
    def set_combo_box_default(self, ch1, ch2):
        if ch1 == 1:
            self.comboBox_output_ch1.setCurrentText("ON")
        else:
            self.comboBox_output_ch1.setCurrentText("OFF")
        if ch2 == 1:
            self.comboBox_output_ch2.setCurrentText("ON")
        else:
            self.comboBox_output_ch2.setCurrentText("OFF")


    # Set upper LED color green
    def set_LED_connected(self, val):
        if val == True:
            self.LED_connected.setStyleSheet(color_green)
            self.ready = True
        elif val == False:
            self.LED_connected.setStyleSheet(color_grey)
            self.ready = False


    # Submit settings for channel 1
    def submit_configs_ch1(self):
        output = self.comboBox_output_ch1.currentText()

        f = self.value_frequency_ch1.text()
        a = self.value_amplitude_ch1.text()
        p = self.value_phase_ch1.text()
        
        if f == "":
            f = self.value_frequency_ch1.placeholderText()
        if a == "":
            a = self.value_amplitude_ch1.placeholderText()
        if p == "":
            p = self.value_phase_ch1.placeholderText()
        
        # Check if input convertable to float, i.e. check if input is numbers
        try:
            f_temp = float(f.replace(",", "."))
            a_temp = float(a.replace(",", "."))
            p_temp = float(p.replace(",", "."))
        
            frequency = str(f_temp*1e6)
            amplitude = str(a_temp/1000)
            phase = str(p_temp)

            # Hand input to worker thread
            self.worker.set_values_ch1(output, frequency, amplitude, phase)
        
        except:
            print("\n*****Wrong input type for channel 1!***\n")
        
        finally:
            # Clear input values from GUI
            self.value_frequency_ch1.clear()
            self.value_amplitude_ch1.clear()
            self.value_phase_ch1.clear()
    
   

    # Submit settings for channel 2
    def submit_configs_ch2(self):
        output = self.comboBox_output_ch2.currentText()

        f = self.value_frequency_ch2.text()
        a = self.value_amplitude_ch2.text()
        p = self.value_phase_ch2.text()

        if f == "":
            f = self.value_frequency_ch2.placeholderText()
        if a == "":
            a = self.value_amplitude_ch2.placeholderText()
        if p == "":
            p = self.value_phase_ch2.placeholderText()

        # Check if input convertable to float, i.e. check if input is numbers
        try:
            f_temp = float(f.replace(",", "."))
            a_temp = float(a.replace(",", "."))
            p_temp = float(p.replace(",", "."))

            frequency = str(f_temp*1e6)
            amplitude = str(a_temp/1000)
            phase = str(p_temp)

            # Hand input to worker thread
            self.worker.set_values_ch2(output, frequency, amplitude, phase)

        except:
            print("\n*****Wrong input type for channel 2!***\n")
        
        finally:
            # Clear input values from GUI
            self.value_frequency_ch2.clear()
            self.value_amplitude_ch2.clear()
            self.value_phase_ch2.clear()



    # Update values from reading process

    def update_output_ch1(self, val):
        if val == 1:
            self.LED_output_ch1.setStyleSheet(color_green)
        else:
            self.LED_output_ch1.setStyleSheet(color_red)



    def update_output_ch2(self, val):
        if val == 1:
            self.LED_output_ch2.setStyleSheet(color_green)
        else:
            self.LED_output_ch2.setStyleSheet(color_red)



    def update_waveform_ch1(self, val):
        if val == 0.:
            pass
        else:
            self.value_waveform_ch1.setText(str(val))


    def update_waveform_ch2(self, val):
        if val == 0.:
            pass
        else:
            self.value_waveform_ch2.setText(str(val))



    def update_frequency_ch1(self, val):
        if val == 0.:
            pass
        else:
            self.value_frequency_ch1.setPlaceholderText(str(val))


    def update_frequency_ch2(self, val):
        if val == 0.:
            pass
        else:
            self.value_frequency_ch2.setPlaceholderText(str(val))



    def update_amplitude_ch1(self, val):
        if val == 0.:
            pass
        else:
            self.value_amplitude_ch1.setPlaceholderText(str(val))



    def update_amplitude_ch2(self, val):
        if val == 0.:
            pass
        else:
            self.value_amplitude_ch2.setPlaceholderText(str(val))



    def update_phase_ch1(self, val):
        self.value_phase_ch1.setPlaceholderText(str(val))



    def update_phase_ch2(self, val):
        self.value_phase_ch2.setPlaceholderText(str(val))


###################################################################################################

# Main program


# Connect to device
tcpIp = '192.168.0.117'
tcpPort = 5024
buffer_size = 1024
dev = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
dev.connect((tcpIp, tcpPort))



# If writing log file enabled, check if file exists and write head
if write_log == True:
    if outfile:
        print("Log file open: " + str(filename))

    if os.stat("./fg_log_files/" + filename).st_size == 0:
        print("Writing head to log file...")
        outfile.write("#Format:\n\n")
        outfile.write("#User input:\n")
        outfile.write("#Time\tChannel\tFrequency (MHz)\tAmplitude (mV)\tPhase (deg)\tOutput ON\n\n")
        outfile.write("#Current settings:\n")
        outfile.write("#Time\tChannel\tFrequency (MHz)\tAmplitude (mV)\tPhase (deg)\tOutput ON\n")
        outfile.write("#Time\tChannel\tFrequency (MHz)\tAmplitude (mV)\tPhase (deg)\tOutput ON\n\n")



# Start GUI
if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())





